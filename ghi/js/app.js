
window.addEventListener('DOMContentLoaded', async () => {


    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString();
                    const endDate = new Date(details.conference.ends).toLocaleDateString();
                    const locationName = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);

                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }

            }




        }
        } catch (error) {
            console.error('error', error);
            console.log(e);
        }

    });

    function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
        return `
            <div class="col">
                <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
                        <p class="card-text">${description}</p>
                    </div>
                    <div class="card-footer">
                    <small class="text-body-scondary">${startDate} - ${endDate}</small>
                    </div>
                </div>
            </div>
        `;
        }
