import React, {useEffect, useState} from 'react';

function ConferenceForm() {
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

const nameChanged = (event) => {
    setName(event.target.value)
}
const startsChanged = (event) => {
    setStarts(event.target.value)
}
const endsChanged = (event) => {
    setEnds(event.target.value)
}
const descriptionChanged = (event) => {
    setDescription(event.target.value)
}
const maxPresentationsChanged = (event) => {
    setMaxPresentations(event.target.value)
}
const maxAttendeesChanged = (event) => {
    setMaxAttendees(event.target.value)
}
const locationChanged = (event) => {
    setLocation(event.target.value)
}

const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = location;

    console.log(data)
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation([]);
        }
}
const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json()
        setLocations(data.locations)
        console.log(data.locations)
    }
}
useEffect(() => {
    fetchData();
}, []);

return (
    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={nameChanged} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={startsChanged} value={starts} placeholder="starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={endsChanged} value={ends} placeholder="ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={descriptionChanged} value={description} placeholder="description" required type="text" name="description" id="description" className="form-control"/>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={maxPresentationsChanged} value={maxPresentations} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={maxAttendeesChanged} value={maxAttendees} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={locationChanged} value={location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>
)
}
export default ConferenceForm
